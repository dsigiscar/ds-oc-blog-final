import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {Utilisateur} from '../../models/Utilisateur.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  nextPage: string;
  authForm: FormGroup;

  constructor(private formBuilder: FormBuilder, private _authService: AuthService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.authForm = this.formBuilder.group( {
      nom: ['', Validators.required],
      prenom: ['', Validators.required]
    });
    this.nextPage = '/' + this.route.snapshot.paramMap.get('next');

  }

  submit() {
    const utilisateur: Utilisateur = {
      nom:    <string>this.authForm.get('nom').value,
      prenom: <string>this.authForm.get('prenom').value
    }

    this._authService.connexion(utilisateur);
    if (this.nextPage !== '') {
      console.log(this.nextPage);
      this.router.navigate([this.nextPage]);
    } else {
      this.router.navigate(['/posts']);
    }

  }

}
