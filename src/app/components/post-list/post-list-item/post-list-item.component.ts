import {Component, Input, OnInit} from '@angular/core';
import {PostService} from '../../../services/post.service';
import {PostItem} from '../../../models/PostItem.model';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent implements OnInit {
  @Input() guidPost: string;
  post: PostItem;

  constructor(private _postService: PostService) { }

  ngOnInit() {
    this.post = this._postService.getPost(this.guidPost);
  }
  onDeletePost(): void {
    this._postService.deletePost(this.post);
  }
  onLikePost(): void {
    this._postService.likePost(this.post.id);
    this._postService.savePosts();
    // this.post = this._postService.getPost(this.guidPost);
  }
  onDontLikePost(): void {
    this._postService.dontLikePost(this.post.id);
    // this.post = this._postService.getPost(this.guidPost);
    this._postService.savePosts();
  }

}
