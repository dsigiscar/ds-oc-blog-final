export interface PostItem {
  id?:          string;
  titre:        string;
  date?:        string;
  auteur?:       string;
  contenu:      string;
  likes?:       number;
  dontLikes?:   number;
}
