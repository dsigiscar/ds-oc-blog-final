import { Injectable } from '@angular/core';
import {Utilisateur} from '../models/Utilisateur.model';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  utilisateurConnecte: Utilisateur;
  isAuth: boolean;
  utilisateurConnecteSubect = new Subject<Utilisateur>();
  constructor() {
    this.isAuth = false;
  }
  connexion(utilisateur: Utilisateur) {
    this.utilisateurConnecte = utilisateur;
    this.isAuth = true;
    this.utilisateurConnecteSubect.next(this.utilisateurConnecte);
    return true;
  }
  deconnexion() {
    this.utilisateurConnecte = null;
    this.isAuth = false;
  }
}
