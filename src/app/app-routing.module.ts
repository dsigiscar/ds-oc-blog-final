import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BlogComponent} from './pages/blog/blog.component';
import {NewPostComponent} from './pages/new-post/new-post.component';
import {PageNotFoundComponent} from './pages/page-not-found/page-not-found.component';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthComponent} from './pages/auth/auth.component';

const routes: Routes = [
  { path: 'posts', component: BlogComponent },
  { path: 'connexion/:next', component: AuthComponent },
  { path: 'new', canActivate: [AuthGuardService], component: NewPostComponent },
  { path: '',
    redirectTo: 'posts',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}
