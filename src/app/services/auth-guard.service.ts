import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private _authService: AuthService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(
      (resolve, reject) => {
        if (this._authService.isAuth) {
          resolve(true);
        } else {
          let nextPage = '/connexion';
          if (state.url !== '/connexion') {
            nextPage = nextPage + state.url;
          }
          this.router.navigate([nextPage]);
          resolve(false);
        }
      });
  }
}
