import { Injectable } from '@angular/core';
import {Subject} from 'rxjs';
import {PostItem} from '../models/PostItem.model';
import * as firebase from 'firebase';
import {AuthService} from './auth.service';
import {Guid} from 'guid-typescript';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  postsList: PostItem[];
  listPostSubject = new Subject<PostItem[]>();

  constructor(private _authService: AuthService) {
    this.listPostSubject.next(this.postsList);
  }
  emitt(): void {
    this.listPostSubject.next(this.postsList);
  }
  // initialise la liste des posts
  initPosts(): void {
    firebase.database().ref('/posts')
      .on('value', (data) => {
        this.postsList = data.val() ? data.val() : [];
        this.emitt();
      });
  }
  // Créer un nouveau post
  createNewPost(post: PostItem): void {
    post.date       =     firebase.firestore.Timestamp.now().toDate().toDateString();
    post.auteur     =     this._authService.utilisateurConnecte.prenom + this._authService.utilisateurConnecte.nom;
    post.id         =     Guid.create().toString();
    post.likes      =     0;
    post.dontLikes  =     0;
    this.postsList.push(post);
    this.savePosts();
    this.emitt();
  }
  // Renvoi un post à partir de son identifiant
  getPost(guidPost: string): PostItem {
    const index = this.getPostIndexFromGuid(guidPost);
    return this.postsList[index];
  }
  // Incremente les likes d'un post de 1
  likePost(guidPost: string): void {
    const index = this.getPostIndexFromGuid(guidPost);
    this.postsList[index].likes = this.postsList[index].likes + 1;
    this.emitt();
  }
  // Incremente les dontLikes d'un post de 1
  dontLikePost(guidPost: string): void {
    const index = this.getPostIndexFromGuid(guidPost);
    this.postsList[index].dontLikes = this.postsList[index].dontLikes + 1;
    this.emitt();
  }
  // Supprime un post
  deletePost(post: PostItem): void {
    const index = this.getPostIndexFromGuid(post.id);
    this.postsList.splice(index, 1);
    this.savePosts();
    this.emitt();
  }
  // Met à jour la liste des posts en base de données
  savePosts(): void {
    firebase.database().ref('/posts/').set(this.postsList);
  }
  // Récupère l'index d'un post à partir de son guid
  getPostIndexFromGuid(guid: string): number {
    const index = this.postsList.findIndex(
      (postElt: PostItem) => {
        if (postElt.id === guid) {
          return true;
        }});
    return index;
  }
}
