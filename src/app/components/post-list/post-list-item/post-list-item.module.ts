import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PostListItemComponent} from './post-list-item.component';
import {MatListModule} from '@angular/material';

@NgModule({
  declarations: [
    PostListItemComponent
  ],
  imports: [
    CommonModule,
    MatListModule,
  ],
  exports: [
    PostListItemComponent
  ]
})
export class PostListItemModule { }
