import {Component, OnDestroy, OnInit} from '@angular/core';
import {PostItem} from '../../models/PostItem.model';
import {Subscription} from 'rxjs';
import {PostService} from '../../services/post.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {

  postsList: PostItem[];
  postsListSubscription: Subscription;

  constructor(private _postService: PostService) { }

  ngOnInit() {
    this.postsListSubscription = this._postService.listPostSubject.subscribe(
      (postsList: PostItem[]) => {
        this.postsList = postsList;
      });
    this._postService.initPosts();
  }
  ngOnDestroy() {
    this.postsListSubscription.unsubscribe();
  }

}
