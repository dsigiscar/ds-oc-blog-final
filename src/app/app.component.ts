import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'untitled';
  constructor() {
    var firebaseConfig = {
      apiKey: 'AIzaSyCE9tUhCCqeZnKL48ILYBOvrSsrjWWaf8o',
      authDomain: 'oc-ex-final.firebaseapp.com',
      databaseURL: 'https://oc-ex-final.firebaseio.com',
      projectId: 'oc-ex-final',
      storageBucket: 'oc-ex-final.appspot.com',
      messagingSenderId: '473390371332',
      appId: '1:473390371332:web:dde754d6ed3cbacd'
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }
}
