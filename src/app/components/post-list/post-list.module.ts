import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PostListComponent} from './post-list.component';
import {PostListItemModule} from './post-list-item/post-list-item.module';
import {MatListModule} from '@angular/material';

@NgModule({
  declarations: [
    PostListComponent,
  ],
  imports: [
    CommonModule,
    MatListModule,
    PostListItemModule,
  ],
  exports: [
    PostListComponent
  ]
})
export class PostListModule { }
