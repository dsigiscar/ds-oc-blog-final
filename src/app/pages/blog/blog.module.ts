import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {BlogComponent} from './blog.component';
import {PostListModule} from '../../components/post-list/post-list.module';

@NgModule({
  declarations: [
    BlogComponent
  ],
  imports: [
    CommonModule,
    PostListModule
  ],
  exports: [
    BlogComponent
  ]
})
export class BlogModule { }
