import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {PostService} from '../../services/post.service';
import {PostItem} from '../../models/PostItem.model';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {
  postForm: FormGroup;
  constructor(private _postService: PostService,
              private _authService: AuthService,
              private formBuilder: FormBuilder,
              private router: Router) {}

  ngOnInit() {
    this.postForm = this.formBuilder.group( {
      titre: ['', Validators.required],
      contenu: ['', Validators.required]
    });
  }

  submit() {
    const post: PostItem = {
      titre:    <string>this.postForm.get('titre').value,
      contenu:  <string>this.postForm.get('contenu').value
    };

    this._postService.createNewPost(post);
    this.router.navigate(['/posts']);
  }

}
