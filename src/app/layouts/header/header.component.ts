import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  title = 'OpenClassroom blog';

  constructor(private breakpointObserver: BreakpointObserver, private _authService: AuthService) {}

  deconnecter() {
    this._authService.deconnexion();
  }

}
