import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {PostService} from './services/post.service';
import {AuthGuardService} from './services/auth-guard.service';
import {AuthService} from './services/auth.service';
import {BlogModule} from './pages/blog/blog.module';
import {NewPostModule} from './pages/new-post/new-post.module';
import {PageNotFoundModule} from './pages/page-not-found/page-not-found.module';
import {HeaderModule} from './layouts/header/header.module';
import {AuthModule} from './pages/auth/auth.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    // -- Layouts
    // ---------
    HeaderModule,       // Header NaviHeaderModulegation
    // -- Pages
    // ---------
    AuthModule,         // Connexion de l'utilisateur
    BlogModule,         // Liste des publications
    NewPostModule,      // Création d'un nouvelle publication
    PageNotFoundModule  // Page non trouvé
  ],
  providers: [
    PostService,
    AuthGuardService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
